cocoon/arm-grafana   FAILED
===================

based on repositories

* https://github.com/appcelerator/docker-alpine
* https://github.com/appcelerator/docker-grafana



#build the image for alpine on arm

in the dockerfile change 
    
    FROM alpine:latest
to

    FROM armhf/alpine:latest


##clone this repository on a raspberry pi

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/docker-arm.git docker-arm
    cd docker-arm/grafana/alpine
    docker build -t cocoon/arm-grafana .
    
    
##push the image
    
    
    docker push cocoon/arm-grafana
    
    