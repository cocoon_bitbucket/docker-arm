
# Start Carbon daemon and Django application in developement mode
 CMD /opt/graphite/bin/carbon-cache.py start && \
     PYTHONPATH=/opt/graphite/webapp DJANGO_SETTINGS_MODULE=graphite.settings python /usr/local/bin/post-setup-graphite-web.py --debug && \
     PYTHONPATH=/opt/graphite/whisper /opt/graphite/bin/run-graphite-devel-server.py --libs=/opt/graphite/webapp/ /opt/graphite/
