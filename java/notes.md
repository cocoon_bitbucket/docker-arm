build a java docker image from official  docker image

see: https://hub.docker.com/_/openjdk/


based on repository: https://github.com/docker-library/openjdk



#build the image for alpine on arm

in the dockerfile change 
    
    FROM alpine:3.4
to

    FROM armhf/alpine:3.4


##clone this repository on a raspberry pi

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/docker-arm.git docker-arm
    cd docker-arm/java/7-jdk/alpine
    docker build -t cocoon/arm-java .
    
    
##push the image
    
    
    docker push cocoon/arm-java
    
    
 
    
    
