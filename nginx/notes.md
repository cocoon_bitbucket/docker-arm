build a nginx docker image from official alpine docker image

see: https://hub.docker.com/_/nginx/


based on repository: https://github.com/nginxinc/docker-nginx



#build the image for alpine nginx on arm

in the dockerfile chage 
    
    FROM alpine:3.4
to

    FROM armhf/alpine:3.4


##clone this repository on a raspberry pi

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/docker-arm.git docker-arm
    cd docker-arm/nginx/alpine
    docker build -t cocoon/arm-nginx .
    
    
##push the image
    
    
    docker push cocoon/arm-nginx
    
    
 
    
    
