build a nginx docker image from official alpine docker image

see: https://hub.docker.com/_/nginx/


based on repository: https://github.com/docker-library/redis



#build the image for alpine on arm

in the dockerfile change 
    
    FROM alpine:3.5
to

    FROM armhf/alpine:3.4


##clone this repository on a raspberry pi

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/docker-arm.git docker-arm
    cd docker-arm/redis/3.0/alpine
    docker build -t cocoon/arm-redis .
    
    
##push the image
    
    
    docker push cocoon/arm-redis
    
    
 
    
    
